// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "RoomEscape.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	Owner = GetOwner();
	ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();
	ClosedRotator = Owner->GetActorRotation();
	OpenRotator = ClosedRotator + FRotator(0.f, OpenAngle, 0.f);	
}

void UOpenDoor::OpenDoor()
{
	//FRotator NewRotation = FRotator(0.0f, OpenAngle, 0.0f);
	Owner->SetActorRotation(OpenRotator);
}

void UOpenDoor::CloseDoor()
{
	//FRotator NewRotation = FRotator(0.0f, 0.0f, 0.0f);
	Owner->SetActorRotation(ClosedRotator);
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	float CurrentTime = GetWorld()->GetTimeSeconds();

	//Poll the trigger volume
	//If the ActorThatOpens is in the volume
	if (PressurePlate->IsOverlappingActor(ActorThatOpens))
	{
		OpenDoor();
		LastDoorOpenTime = CurrentTime;
	}
	else	//Check to see if the door needs to be closed
	{
		float TimeDiff = CurrentTime - LastDoorOpenTime;
		if (TimeDiff > DoorCloseDelay)
			CloseDoor();
	}
	
}	

